'use strict'
const User = use('App/Models/User');

class UserController {

    async login({ request, auth, response }) {
        const { email, password } = request.all()
        try {
            if (await auth.attempt(email, password)) {
              let user = await User.findBy('email', email)
              let accessToken = await auth.generate(user)
              return response.json({"user":user, "access_token": accessToken})
            }
          }
          catch (e) {
            return response.json({message: 'You first need to register!'})
          }
    }

    async register({request, auth, response}) {
        const username = request.input("username")
        const email = request.input("email")
        const password = request.input("password")

        let user = new User()
        user.username = username
        user.email = email
        user.password = password

        console.log("request");
        user = await user.save()
        console.log("done");
        let accessToken = await auth.generate(user)
        return response.json({"user": user, "access_token": accessToken})
}

    show({ auth, params }) {
        if (auth.user.id !== Number(params.id)) {
            return 'You cannot see someone else\'s profile'
        }
        return auth.user
    }

}

module.exports = UserController
